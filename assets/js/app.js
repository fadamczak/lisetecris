var titre = "";
var consigne = "";

var tEleves = {};
var tNoms = [];

var motEnCours = "";
var tMotEnCours = [];

$(function(){

	getMarkdownContent();

	$('#fenetreDeJeu').hide();

	$('#lancer').on('click', function(){
		lancerActivite();
		$('#fenetreDeJeu').show();
	});

	$('#btLu').on('click', function(){
		$('#box-motARecopier').hide();
		$('#box-propositionEleve').show();
	});

	$('#btEcris').on('click', function(){
		let proposition = $('#propositionEleve').val();
		validerProposition(motEnCours, proposition);
	});

})

function getMarkdownContent() {
	// Récupération du markdown externe
	let urlMD = window.location.hash.substring(1); // Récupère l'URL du hashtag sans le #

	if (urlMD !== "") {
		// Gestion des fichiers hébergés sur github
		if (urlMD.startsWith("https://github.com")) {
			urlMD = urlMD.replace(
				"https://github.com",
				"https://raw.githubusercontent.com"
			);
			urlMD = urlMD.replace("/blob/", "/");
		}
		// Gestion des fichiers hébergés sur codiMD
		if (
			urlMD.startsWith("https://codimd") &&
			urlMD.indexOf("download") === -1
		) {
			urlMD =
				urlMD
					.replace("?edit", "")
					.replace("?both", "")
					.replace("?view", "")
					.replace(/#$/, "") + "/download";
		}
		// Gestion des fichiers hébergés via Hedgedoc
		if (
			urlMD.includes("hedgedoc") &&
			urlMD.indexOf("download") === -1
		) {
			urlMD =
				urlMD
					.replace("?edit", "")
					.replace("?both", "")
					.replace("?view", "")
					.replace(/#$/, "") + "/download";
		}

		// Récupération du contenu du fichier
		
		let md = fetch(urlMD)
			.then((response) => response.text())
			.then((data) => {
				construire(data);
			}) 
	
			.catch((error) => console.error(error));
		

	} 

}

function construire(data) {
	//console.log(data);
	//$('#contenuMd').html(data);
	let tData = data.split("\n");
	//console.log(tData);

	var i = 0;

	while (i < tData.length) {

		let ligne = tData[i];
		if (ligne != "") {
			//console.log(ligne);	

			if (ligne.startsWith("# ")) {
				titre = ligne.substring(2);
			}

			if (ligne.startsWith("## ")) {
				consigne = ligne.substring(3);
			}

			if (ligne.startsWith('### ')) {
				nomEleve = ligne.substring(4);
				tNoms.push(nomEleve);
				tEleves[nomEleve] = [];
				let tMots = [];
				++i;
				while(tData[i] != ""){
					tMots.push(tData[i].substring(2));
					++i;
				}
				tEleves[nomEleve] = tMots;
			}


		}
	
		++i;
	}

	initialiser();
	/*
	console.log(titre);
	console.log(consigne);
	console.log(tEleves);
	*/

}

function initialiser() {

	$('#consigne').text(consigne);
	$('#titre').text(titre);

	for(i=0; i < tNoms.length; ++i) {
		$("#listeEleves").append($('<option>', {
			value : tNoms[i],
			text : tNoms[i]
		}));
	}

}

function lancerActivite() {
	eleveEnCours = $('#listeEleves').val();
	//console.log(tEleves[eleveEnCours]);
	tMots = tEleves[eleveEnCours].sort(() => Math.random() - 0.5);
	//console.log(tMots);
	proposerMot();
}

function proposerMot(){
	if(tMots.length > 0) {
		motEnCours = tMots.shift();
		$('#motARecopier').text(motEnCours);
		$('#box-propositionEleve').hide();	
	} else {
		alert('TERMINE');
		$('#fenetreDeJeu').hide();
		//lancerActivite();
	}
	
	//console.log(motEnCours);
}

function validerProposition(motEnCours, proposition) {
	if(motEnCours == proposition) {
		alert('BRAVO');
	} else {
		alert('Erreur');
	}
	$('#propositionEleve').val("");

	$('#box-propositionEleve').hide();
	$('#box-motARecopier').show();

	proposerMot();
}
